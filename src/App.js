import React, {Component} from 'react';
import Register from './pages/Register/Register'
import EditProfile from './pages/EditProfile/EditProfile'
import {Route} from 'react-router-dom'
import Navbar from './Components/Nav'
import Login from './pages/Login/Login'
import LoginDialog from './Components/LoginDialog'
import Notification from './pages/Notification'
import History from './pages/History';
import Profile from "./pages/Profile";
import CPWD from './pages/ChangePassword'
import Fav from './pages/Favorite'
import Interest from './pages/Interest'
import './App.css';
import 'typeface-kanit'
import 'normalize.css'

class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      loginDialog: false,
    }
  }

  toggleLoginDialog = e => {
    this.setState({
      loginDialog: !this.state.loginDialog
    })
  }

  render() {
    const {loginDialog} = this.state
    return (
      <div className="App">
        <Navbar login={this.toggleLoginDialog}/>
        <LoginDialog handleClose={this.toggleLoginDialog} open={loginDialog}/>
        <Route path="/register" component={Register}/>
        <Route path="/login" component={Login}/>
        <Route path="/editProfile" component={EditProfile}/>
        <Route path="/profile" component={Profile}/>
        <Route path="/history" component={History}/>
        <Route path="/changePassword" component={CPWD}/>
        <Route path="/favorite" component={Fav}/>
        <Route path="/editInterest" component={Interest}/>
        <Route path="/notification" component={Notification}/>
      </div>
    );
  }
}

export default App;
