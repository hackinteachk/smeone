import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Grid from '@material-ui/core/Grid';
import Toolbar from '@material-ui/core/Toolbar';
import logo from '../static/img/logo.png'
import Typography from '@material-ui/core/Typography';
import {fade} from '@material-ui/core/styles/colorManipulator';
import SearchIcon from '@material-ui/icons/Search';
import Input from '@material-ui/core/Input'
import NotificationOutlined from '@material-ui/icons/NotificationsOutlined';
import NotificationRounded from '@material-ui/icons/NotificationsRounded';
import {Link} from "react-router-dom";
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import Grow from '@material-ui/core/Grow';
import Paper from '@material-ui/core/Paper';
import Popper from '@material-ui/core/Popper';
import IconButton from '@material-ui/core/IconButton';
import {withRouter} from 'react-router-dom'
import {Card, Divider, List, ListItem} from '@material-ui/core/';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import {ListItemText, ButtonBase, MenuList, Button,ListItemSecondaryAction, MenuItem} from '@material-ui/core'

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  toolbar: {
    margin: theme.spacing.unit * 2,
  },
  linkBar: {
    margin: theme.spacing.unit,
  },
  search: {
    position: 'relative',
    backgroundColor: fade(theme.palette.common.white, 0.15),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.white, 0.25),
    },
    marginRight: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing.unit,
      width: 'auto',
    },
  },
  searchIcon: {
    width: theme.spacing.unit * 9,
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputRoot: {
    color: 'inherit',
    width: '100%',
  },
  inputInput: {
    paddingTop: theme.spacing.unit,
    paddingRight: theme.spacing.unit,
    paddingBottom: theme.spacing.unit,
    paddingLeft: theme.spacing.unit * 10,
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      width: 120,
      '&:focus': {
        width: 200,
      },
    },
  },
  space: {
    marginLeft: theme.spacing.unit * 2,
  },
  linkText: {
    'font-family': 'kanit',
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit
  }
});

const navMenu = [
  {
    txt: "เงินทุน",
    to: "#",
    key: "fund"
  },
  {
    txt: "นวัตกรรม",
    to: "#",
    key: "inno",
  },
  {
    txt: "เพิ่มยอดขาย",
    to: "#",
    key: "increaseSale"
  },
  {
    txt: "เริ่มต้นธุรกิจ/START UP",
    to: "#",
    key: "startup",
  },
  {
    txt: "แหล่งความรู้",
    to: "#",
    key: "knowledge"
  },
  {
    txt: "ข่าวสาร/กิจกรรม",
    to: "#",
    key: "news"
  },
  {
    txt: "สิทธิประโยชน์/บริการ SME",
    to: "#",
    key: "advantages"
  }
];

const NotificationButton = ({open, ...rest}) => {
  return (
    <IconButton aria-label="Notification" {...rest}>
      {
        !!open && <NotificationRounded/>
      }
      {
        !open && <NotificationOutlined/>
      }
    </IconButton>
  )
}

const ProfileButton = withStyles({
  root: {
    fontFamily: 'kanit',
    '&:hover':{
      backgroundColor: 'rgba(0,0,0,0)'
    }
  }
})(ButtonBase)

const st = {
  card: {
    maxWidth: 345,
    borderRadius: 10,
  },
  media: {
    objectFit: 'cover',
  },
  listItem: {
    '&:hover': {
      backgroundColor: 'rgba(0,0,0,0)',
    },
  },
  typo: {
    '&:hover': {
      color: 'black'
    }
  },
  hover: {
    '&:hover': {
      textDecorationColor: 'black'
    }
  },
  itemText: {
    '&:hover': {
      color: '#494949'
    }
  }
};

const ReadListItemText = withStyles({
  root: {
    fontFamily: 'kanit',
    color: '#494949',
    '&:hover': {
      color: 'black'
    }
  },
})(ListItemText);

const UnreadListItemText = withStyles({
  root: {
    fontFamily: 'kanit',
    color: '#70AB95',
    '&:hover': {
      color: 'black'
    }
  },
})(ListItemText);

const NavListItem = withStyles({
  root: {
    '&:hover': {
      backgroundColor: 'rgba(0,0,0,0)'
    }
  },
})(ListItem);

let NotificationMenu = withStyles(st)(({history, handleClose, classes, notif}) => (
  <Card className={classes.card}>
    <CardContent
    >
      <Grid container spacing={24} justify={"space-between"} direction={"row"}>
        <Grid item>
          <Typography variant={"body2"}>
            NOTIFICATIONS
          </Typography>
        </Grid>
        <Grid item>
          <Typography
            onClick={(e) => {
              history.push('/notification')
              handleClose(e)
            }}
            variant={"body1"} style={{cursor: 'pointer', color: '#70AB95'}}
          >
            ดูทั้งหมด
          </Typography>
        </Grid>
      </Grid>
    </CardContent>
    <CardActions>
      <List>
        <Divider/>
        {Object.keys(notif).map((i) => {
          const {title, read, date} = notif[i];
          return (
            <NavListItem style={{cursor:'pointer'}}>
              {
                read ?
                <ReadListItemText
                  disableRipple={true}
                  primary={title}
                  disableTypography
                  // style={{color:color}}
                  className={{root: classes.itemText}}
                  secondary={date}
                />
                  :
                <UnreadListItemText
                    primary={title}
                    disableRipple={true}
                    disableTypography
                    // style={{color:color}}
                    className={{root: classes.itemText}}
                    secondary={date}
                />
              }
              <ListItemSecondaryAction>
                {!read &&
                <div style={{
                  display: 'block',
                  backgroundColor: '#70AB95',
                  height: '10px',
                  width: '10px',
                  borderRadius: '50%',
                  marginRight: '1em'
                }}/>}
              </ListItemSecondaryAction>
            </NavListItem>
          )
        })}
      </List>
    </CardActions>
  </Card>
));

NotificationMenu = withRouter(NotificationMenu);

const NotificationPanel = ({props, handleClose, anchorEl, notif}) => {
  return (
    <Popper
      placement="bottom-end"
      disablePortal={false}
      modifiers={{
        flip: {
          enabled: true,
        },
        preventOverflow: {
          enabled: true,
          boundariesElement: 'scrollParent',
        },
        arrow: {
          enabled: false,
        },
      }}
      open={Boolean(anchorEl)} anchorEl={anchorEl} transition disablePortal>
      {({TransitionProps, placement}) => (
        <Grow
          {...TransitionProps}
          style={{transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom'}}
        >
          <Paper style={{borderRadius: '20px'}}>
            <ClickAwayListener onClickAway={handleClose}>
              <NotificationMenu props={props} notif={notif} handleClose={handleClose}/>
            </ClickAwayListener>
          </Paper>
        </Grow>
      )}
    </Popper>
  )
}

class Notificator extends React.Component {
  state = {
    show: this.props.show,
    anchorEl: this.props.anchorEl,
    notif: [
      {
        title: 'สัมนา รู้ก่อนใคร! เทรน SME ...',
        date: '20 กรกฎาคม 2561 ',
        read: false
      },
      {
        title: "บอกเทคนิคเตรียมความพร้อมก่อนลุย...",
        date: '17 กรกฎาคม 2561',
        read: false,
      },
      {
        title: 'สัมนา รู้ก่อนใคร! เทรน SME ...',
        date: '1 กรกฎาคม 2561',
        read: true
      }
    ]
  }

  static propTypes = {
    show: PropTypes.bool
  }

  static defaultProps = {
    show: false,
    anchorEl: null
  }

  toggleNotificator = (e) => {
    const target = e.currentTarget;
    this.setState((prev) => ({anchorEl: (Boolean(prev.anchorEl) ? null : target)}))
  }

  closeNotificator = (e) => {

    if (this.anchorEl.contains(e.target)) {
      return;
    }
    this.setState({anchorEl: null})
  }

  render() {
    const {
      anchorEl, notif
    } = this.state

    return (
      <React.Fragment>
        <NotificationButton buttonRef={node => {
          this.anchorEl = node;
        }} aria-owns={anchorEl ? 'simple-menu' : null} aria-haspopup="true" open={Boolean(anchorEl)}
                            onClick={this.toggleNotificator}/>
        <NotificationPanel notif={notif} handleClose={this.closeNotificator} anchorEl={anchorEl}/>
      </React.Fragment>

    )
  }
}

const MyMenuItem = withStyles({
  root:{
    '&:hover':{
      backgroundColor: 'rgba(0,0,0,0)'
    }
  }
})(MenuItem)

let ProfilePopper = ({history,props, handleClose, anchorEl, profile}) => {
  return (
    <Popper
      placement="bottom-end"
      disablePortal={false}
      modifiers={{
        flip: {
          enabled: true,
        },
        preventOverflow: {
          enabled: true,
          boundariesElement: 'scrollParent',
        },
        arrow: {
          enabled: false,
        },
      }}
      open={Boolean(anchorEl)} anchorEl={anchorEl} transition disablePortal>
      {({TransitionProps, placement}) => (
        <Grow
          {...TransitionProps}
          style={{transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom'}}
        >
          <Paper style={{borderRadius: '20px'}}>
            <ClickAwayListener onClickAway={handleClose}>
              {/*<NotificationMenu props={props} notif={notif} handleClose={handleClose}/>*/}
              <MenuList>
                {
                  Object.keys(profile).map(i => {
                    const {label, link} = profile[i]
                    return (
                      <MyMenuItem
                        disableRipple={true}
                        onClick={()=>{
                          history.push(link)
                          handleClose()
                        }}>
                        {label}
                      </MyMenuItem>
                    )
                  })
                }
              </MenuList>
            </ClickAwayListener>
          </Paper>
        </Grow>
      )}
    </Popper>
  )
}

ProfilePopper = withRouter(ProfilePopper)

class Profiler extends React.Component {
  state = {
    show: this.props.show,
    anchorEl: this.props.anchorEl,
    user: 'ภคกุล',
    profile: [
      {
        label: 'แก้ไขข้อมูลส่วนตัว',
        link: '/editProfile'
      },
      {
        label: 'ออกจากระบบ',
        link: '/logout'
      },
    ]
  }

  static propTypes = {
    show: PropTypes.bool
  }

  static defaultProps = {
    show: false,
    anchorEl: null
  }

  toggleNotificator = (e) => {
    const target = e.currentTarget;
    this.setState((prev) => ({anchorEl: (Boolean(prev.anchorEl) ? null : target)}))
  }

  closeNotificator = (e) => {

    if (this.anchorEl.contains(e.target)) {
      return;
    }
    this.setState({anchorEl: null})
  }

  render() {
    const {
      anchorEl, profile,user
    } = this.state

    return (
      <React.Fragment>
        <ProfileButton
          disableRipple={true}
          buttonRef={node => {
          this.anchorEl = node;
        }} aria-owns={anchorEl ? 'simple-menu' : null} aria-haspopup="true" open={Boolean(anchorEl)}
                       onClick={this.toggleNotificator}>{user}</ProfileButton>
        <ProfilePopper profile={profile} handleClose={this.closeNotificator} anchorEl={anchorEl}/>
      </React.Fragment>

    )
  }
}

function Nav(props) {
  const {classes, login, menu} = props;

  return (
    <div className={classes.root}>
      <AppBar position="static" color={"inherit"} elevation={0}>
        <Toolbar className={classes.toolbar}>
          <img src={logo} height={50} onClick={login}/>
          <div className={classes.root}/>
          <div className={classes.search}>
            <div className={classes.searchIcon}>
              <SearchIcon/>
            </div>
            <Input
              placeholder="Search…"
              disableUnderline
              classes={{
                root: classes.inputRoot,
                input: classes.inputInput,
              }}
            />
          </div>
          <div className={classes.space}/>
          <Profiler/>
          <Notificator/>
        </Toolbar>

        <Grid container className={classes.linkBar} justify={"center"}>
          {navMenu.map(({txt, to, key}) => (
            <Typography className={classes.linkText} align={"center"} key={key}>
              <Link style={{textDecoration: 'none'}} to={to}>{txt}</Link>
            </Typography>
          ))}
        </Grid>
      </AppBar>
    </div>
  );
}


Nav.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(withRouter(Nav));