import axios from 'axios'
const xmlparser = require('js2xmlparser')
const o2x = require('object-to-xml')

const USERNAME = 'boonloed';
const PWD = 'CDDBCBEE32389756F828FE928B952369'
export const getProvince = () => {
  const obj = {
    '?xml version=\"1.0\" encoding=\"utf-8\"?' : null,
    "soap:Envelope": [
      {
        "@": {
          "xmlns:xsi": "http://www.w3.org/2001/XMLSchema-instance",
          "xmlns:xsd": "http://www.w3.org/2001/XMLSchema",
          "xmlns:soap": "http://schemas.xmlsoap.org/soap/envelope/"
        },
        "soap:Header": {
          "AuthHeader":
            {
              "@": {
                "xmlns": "http://tempuri.org"
              },
              "Username": USERNAME,
              "Password": PWD
            }
        },
        "soap:Body": {
          "getTitle" : {
            "@": {
              "xmlns": "http://tempuri.org"
            }
          }
        }
      }
    ]
  }
  console.log(o2x(obj))
  let url="https://members.sme.go.th/osmep_service/masterservice.asmx?op=getTitle"
  return axios.post(url,o2x(obj),
    {
      headers:{
        'Content-Type': 'text/xml',
      }
    })
  .then(({data})=>console.log(data))
}