import React from 'react';
import ReactDOM from 'react-dom';
import 'normalize.css'
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import {BrowserRouter} from 'react-router-dom'
import {MuiThemeProvider, createMuiTheme} from '@material-ui/core/styles';

const theme = createMuiTheme({
  palette: {
    primary: {main: "#494949"},
    secondary: {main: "#70AB95"},
  },
  overrides: {
    MuiButton: { // Name of the component ⚛️ / style sheet
      root: { // Name of the rule
        color: 'white', // Some CSS
        borderRadius: '3em',
      },
    },
    MuiInputLabel: {
      formControl: {
        paddingLeft: '1em',
      }
    },
    MuiSelect: {
      icon:{
        paddingRight: '1em',
      },
      select:
        {
          '&:after': {
            borderRadius: '3em',
          },
          '&:focus': {
            borderRadius: '3em'
          },
          paddingRight: '1em',
        }
    },
    MuiPaper: {
      rounded: {
        borderRadius: '2em',
      }
    },
    MuiInput: {
      inputMultiline:{
        padding: '1em',
      },
      input: {
        border: 'solid 1px rgba(0,0,0,0.5)',
        borderRadius: '2em',
        paddingLeft: '1em',
      },
      underline: {
        '&$disabled:before':{
          borderBottom: '0px'
        },
        '&:hover': {
          '&:before': {
            borderBottom: ['rgba(255, 255, 255, 0.0)', '!important'],
          }
        },
        '&:before': {
          borderBottom: 'rgba(255, 255, 255, 0.0)',
        },
        '&:after': {
          borderBottom: 'rgba(255,255,255,0.0)',
        }
      }
    }
  },
  typography: {
    // Use the system font instead of the default Roboto font.
    fontFamily: [
      '-apple-system',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      'Roboto',
      '"Kanit"',
      '"Helvetica Neue"',
      'Arial',
      'sans-serif',
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(','),
  },
});

const Router = () => (
  <MuiThemeProvider theme={theme}>
    <BrowserRouter>
      <App/>
    </BrowserRouter>
  </MuiThemeProvider>
);


ReactDOM.render(<Router/>, document.getElementById('root'));
registerServiceWorker();
