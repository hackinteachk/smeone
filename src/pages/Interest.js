import React, {Component} from 'react'
import Sidebar from './Sidebar'
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import {
  Grid, Typography,
  Button, Checkbox,
  FormControl, FormControlLabel,
  FormGroup
} from '@material-ui/core'

let options = [
  {text: 'การเงิน', value: 'financial'},
  {text: 'การตลาด', value: 'marketing'},
  {text: 'เทคโนโลยีวัฒนธรรม', value: 'tech'},
  {text: 'พัฒนามาตรฐานสินค้า', value: 'standardise'},
  {text: 'พัฒนาผลิิตภัณฑ์/บรรจุภัณฑ์', value: 'packaging'},
  {text: 'เพิ่มประสิทธิภาพธุรกิิจ', value: 'productivity'},
  {text: 'ผู้ประกอบการใหม่ / START UP', value: 'startup'},
  {text: 'สิทธิประโยชน์/กฎหมาย/ภาษี', value: 'advantages'},
  {text: 'เริ่มต้นธุรกิจใหม่ / START UP', value: 'newbus'},
  {text: 'แหล่งความรู้', value: 'knowledge'},
  {text: 'ข่าวสาร/กิจกรรม', value: 'activity'}
]

let optionsRight = [
  {text: 'ผู้ประกอบการใหม่ / START UP', value: 'startup'},
  {text: 'สิทธิประโยชน์/กฎหมาย/ภาษี', value: 'advantages'},
  {text: 'เริ่มต้นธุรกิจใหม่ / START UP', value: 'newbus'},
  {text: 'แหล่งความรู้', value: 'knowledge'},
  {text: 'ข่าวสาร/กิจกรรม', value: 'activity'}
]

const styles = theme => ({
  root: {},
  formControl: {
    margin: theme.spacing.unit * 3,
  },
});


class Interest extends Component {
  constructor(props) {
    super(props)
    this.state = {
      financial: false,
      marketing: false,
      tech: false,
      standardise: false,
      packaging: false,
      productivity: false,
      startup: false,
      advantages: false,
      newbus: false,
      knowledge: false,
      activity: false,
    }

  }

  handleCheck = name => e => {
    this.setState({
      [name]: e.target.checked,
    })
  }

  render() {
    const {handleCheck} = this
    const {classes} = this.props;
    return (
      <div className={classes.root}>
        <Grid container alignItems={"center"} justify={"center"} style={{marginTop: '3em'}}>
          <Grid item xs={12}>
            <Typography variant={"display1"}>
              แก้ไขประเภทธุรกิจที่สนใจ
            </Typography>
          </Grid>
        </Grid>
        <Grid container alignItems={"center"} style={{padding: '3em'}} spacing={24}>
          <Grid container align={"center"} wrap={"wrap"} justify={"center"}>
            <Grid item xs={4} style={{padding: '2em'}}>
              <Sidebar current={"interest"}/>
            </Grid>
            <Grid item xs={8}>
              <Grid container direction={"row"}
                    spacing={24} style={{padding: '3em'}}
                    justify={"flex-start"}
              >
                <Grid container>
                  {
                    Object.keys(options).map(i => {
                      const {text, value} = options[i]
                      return (
                        <Grid item xs={6} key={i}>
                          <Grid container direction={"row"} wrap={"wrap"} alignItems={"center"}>
                            <Checkbox checked={this.state[value]} onChange={this.handleCheck(value)} value={text}/>
                            <Typography variant={"body1"}>
                              {text}
                            </Typography>
                          </Grid>
                        </Grid>
                      )
                    })
                  }
                </Grid>
              </Grid>
              <Grid container align={"center"} justify={"center"}>
                <Button size={"large"}
                        color={"secondary"}
                        variant={"contained"}
                        style={{margin: '1em', color: 'white'}}
                >
                  บันทึก
                </Button>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </div>
    )
  }
}

Interest.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Interest);