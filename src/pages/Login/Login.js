import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {getProvince} from "../../Components/api";
import {withStyles} from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog'
import DialogContent from '@material-ui/core/DialogContent'
import DialogTitle from '@material-ui/core/DialogTitle'
import Button from '@material-ui/core/Button'
import TextField from '@material-ui/core/TextField'
import DialogActions from '@material-ui/core/DialogActions'
import Grid from '@material-ui/core/Grid'
import {Checkbox, Typography, Icon} from '@material-ui/core'
import {EmailOutlined, LockOutlined} from '@material-ui/icons'
import FormControl from '@material-ui/core/FormControl'

const style = theme => ({
  container: {
    // display: 'flex',
    // flexWrap: 'wrap',
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
  },
  dense: {
    marginTop: 16,
  },
  menu: {
    width: 200,
  },
});

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      email: '',
      password: '',
      keepLogin: false
    }
  }

  componentDidMount(){
    console.log(getProvince())
  }

  handleClose = () => {
    this.setState({
      open: !this.state.open
    })
  }

  Login = () => {
    const {email, password} = this.state
    // @TODO Connect API
    console.log({email,password})
  }

  handleChange = name => e => {
    this.setState({
      [name]: e.target.value
    })
  }

  handleCheck = name => e => {
    this.setState({
      [name]: e.target.checked
    })
  }

  render() {
    const {open, email, password, keepLogin} = this.state
    return (
      <div>
        <Button
          style={{borderRadius:'3em'}}
          onClick={this.handleClose}>Open</Button>
        <Dialog
          style={{borderRadius:'3em'}}
          fullWidth
          maxWidth={"sm"}
          open={open}
          aria-labelledby="form-dialog-title"
          onClose={this.handleClose}>
          <DialogTitle style={{"textAlign": "center"}}>เข้าสู่ระบบ</DialogTitle>
          <DialogContent style={{"padding": "1em"}}>
            <Grid container align={"center"} justify={"center"}>
              <Grid item xs={8}>
                <Grid container align={"center"} justify={"center"} spacing={8} alignItems="flex-end">
                  <Grid item>
                    <EmailOutlined/>
                  </Grid>
                  <Grid item xs={8}>
                    <FormControl fullWidth>
                      <TextField margin={"dense"}
                                 value={email}
                                 fullWidth
                                 style={{minWidth: '100%'}}
                                 label={"อีเมล"}
                                 variant="outlined"
                                 onChange={this.handleChange("email")}
                      />
                    </FormControl>
                  </Grid>
                </Grid>
                <Grid container align={"center"} justify={"center"} spacing={8} alignItems="flex-end">
                  <Grid item>
                    <LockOutlined/>
                  </Grid>
                  <Grid item xs={8}>
                    <FormControl fullWidth>
                      <TextField margin={"dense"}
                                 value={password}
                                 fullWidth
                                 label={"รหัสผ่าน"}
                                 variant="outlined"
                                 type={"password"}
                                 onChange={this.handleChange("password")}
                      />
                    </FormControl>
                  </Grid>
                </Grid>

                <Grid container direction={"row"} align={"center"} justify={"center"}>
                  <Grid item>
                    <Checkbox id={"checkbox"} checked={keepLogin} onChange={this.handleCheck("keepLogin")}/>
                  </Grid>
                  <Grid item>
                    <Typography
                      style={{fontFamily: 'kanit', marginTop: '1em'}}
                      variant={"body1"}
                      gutterBottom={false}
                    >
                      จำรหัสผ่าน
                    </Typography>
                  </Grid>
                </Grid>
                <Grid container wrap={"nowrap"} direction={"column"} align={"center"} justify={"center"}>
                  <Grid item>
                    <Button variant="flat"
                            style={{width:'90%',backgroundColor: "#70AB95", color: "white"}}
                            onClick={this.Login}
                    >ยืนยัน</Button>
                  </Grid>
                  <Grid item>
                    <Typography variant={"caption"} style={{marginTop:'1em'}}>
                      ลืมรหัสผ่าน?
                    </Typography>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </DialogContent>
          <DialogActions>
          </DialogActions>
        </Dialog>
      </div>
    )
  }
}

Login.propTypes = {
  classes: PropTypes.object.isRequired
}

export default withStyles(style)(Login);