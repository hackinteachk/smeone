import React, {Component} from 'react'
import Sidebar from './Sidebar'
import {Grid, Typography, Button, Chip, TextField} from '@material-ui/core'

class ChangePassword extends Component {
  constructor(props) {
    super(props)
    this.state = {
      email: 'aaa@aaa.com',
      password: '',
      confirmPassword: '',
    }
  }

  handleChange = name => e => {
    this.setState({
      [name]: e.target.value,
    })
  }

  render() {
    const {email, password, confirmPassword} = this.state;
    const {handleChange} = this
    return (
      <div>
        <Grid container alignItems={"center"} justify={"center"} style={{marginTop: '3em'}}>
          <Grid item xs={12}>
            <Typography variant={"display1"}>
              เปลี่ยนรหัสผ่าน
            </Typography>
          </Grid>
        </Grid>
        <Grid container alignItems={"center"} style={{padding: '3em'}} spacing={24}>
          <Grid container align={"center"} wrap={"wrap"} justify={"center"}>
            <Grid item xs={4} style={{padding: '2em'}}>
              <Sidebar current={"password"}/>
            </Grid>
            <Grid item xs={8}>
              <Grid container direction={"row"}
                    spacing={24} style={{padding: '3em'}}>
                <Grid item xs={12} style={{margin: '1em'}}>
                  <Typography variant={"headline"} style={{textAlign: 'left'}}>
                    ข้อมูลการเข้าใช้ระบบ
                  </Typography>
                </Grid>
                <Grid item xs={12} container>
                  <TextField
                    style={{margin: '1em'}}
                    label={"อีเมล*"}
                    disabled
                    variant={"outlined"}
                    value={email}
                    onChange={handleChange("email")}
                  />
                </Grid>
                <Grid item container xs={12}>
                  {/*LEFT*/}
                  <Grid item>
                    <form>
                      <TextField
                        style={{margin: '1em'}}
                        value={password}
                        label={"รหัสผ่าน"}
                        variant={"outlined"}
                        type={"password"}
                        onChange={handleChange("password")}
                      />
                      <br/>
                      <TextField
                        style={{margin: '1em'}}
                        value={confirmPassword}
                        variant={"outlined"}
                        label={"ยืนยันรหัสผ่าน"}
                        type={"password"}
                        onChange={handleChange("confirmPassword")}
                      />
                    </form>
                  </Grid>
                  {/*RIGHT*/}
                  <Grid item style={{marginTop:'2em', marginLeft: '2em'}}>
                    <Typography
                      variant={"subheading"}
                      style={{textAlign: 'left', textDecoration: 'underline'}}>
                      คำแนะนำในการตั้งรหัสผ่าน
                    </Typography>
                    <br/>
                    <Typography
                      variant={"caption"}
                      style={{textAlign: 'left'}}
                    >
                      ควรมีความยาวไม่น้อยกว่า 8 ตัวอักษร หรือมากกว่านั้น
                    </Typography>
                    <Typography
                      variant={"caption"}
                      style={{textAlign: 'left'}}
                    >
                      ประกอบด้วยตัวอักษร(a-z,A-Z) / ตัวเลข (0-9)
                    </Typography>
                    <Typography
                      variant={"caption"}
                      style={{textAlign: 'left'}}
                    >
                      เครื่องหมายหรืออักขระพิเศษ (!@#$%^&*()_-=+[]?/.,)
                    </Typography>
                    <Typography
                      variant={"caption"}
                      style={{textAlign: 'left'}}
                    >
                      ตัวอย่างเช่น <a style={{color: '#70AB95'}}>#Admin1234</a>
                    </Typography>
                  </Grid>
                </Grid>
              </Grid>
              <Grid container align={"center"} justify={"center"}>
                <Button size={"large"}
                        color={"secondary"}
                        variant={"contained"}
                        style={{margin:'1em', color:'white'}}
                        >
                  ตกลง
                </Button>
                <Button size={"large"}
                        color={"primary"}
                        variant={"contained"}
                        style={{margin:'1em', color:'white'}}
                >
                  ยกเลิก
                </Button>
              </Grid>
            </Grid>
          </Grid>
        </Grid>

      </div>
    )
  }
}

export default ChangePassword;