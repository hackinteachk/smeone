import React, {Component} from 'react'
import {AccessTime, Edit, FavoriteBorder, LockOutlined, PersonOutlined} from '@material-ui/icons'
import {Divider, Paper, Grid, List, ListSubheader, ListItem, ListItemIcon, ListItemText} from '@material-ui/core'
import {withRouter} from 'react-router-dom'
import {withStyles} from '@material-ui/core/styles';

const NormPaper = withStyles({
  rounded:{
    borderRadius: '0px'
  }
})(Paper)

const menu = [
  {
    label: "บัญชี SME ของคุณ",
    icon: PersonOutlined,
    match: 'account',
    link: '/profile'
  },
  {
    label: 'ประวัติเข้าร่วมโครงการ',
    icon: AccessTime,
    match: 'history',
    link: '/history'
  },
  {
    label: 'เปลี่ยนรหัสผ่าน',
    icon: LockOutlined,
    match: 'password',
    link: '/changePassword'
  },
  {
    label: 'โครงการที่ติดตาม',
    icon: FavoriteBorder,
    match: 'favorite',
    link: '/favorite',
  },
  {
    label: 'แก้ไขประเภทธุรกิจที่สนใจ',
    icon: Edit,
    match: 'interest',
    link: '/editInterest'
  }
]

class Sidebar extends Component {
  constructor(props) {
    super(props)
    this.state = {
      username: "PAKAKOL"
    }
  }

  goto = link => {
    this.props.history.push(link)
  }

  render() {
    const {current} = this.props;
    const {username} = this.state;
    return (
      <NormPaper elevation={0}>
      <Grid container style={{padding: '0 2em'}}>
        <List
          subheader={
            <ListSubheader>{username}, ACCOUNT</ListSubheader>
          }
        >
          <Divider/>
          {Object.keys(menu).map(i => {
            const {label, icon, match, link} = menu[i]
            const $Comp = icon
            return (
              <div key={`${i}-Nav`}>
                <ListItem button>
                  <ListItemIcon>
                    <$Comp style={{color: current === match ? '#70AB95' : ''}}/>
                  </ListItemIcon>
                  <ListItemText
                    style={{color: current === match ? '#70AB95' : '', fontFamily: 'kanit'}}
                    disableTypography={true}
                    onClick={() => this.goto(link)}
                    primary={label}/>
                </ListItem>
                <Divider/>
              </div>
            )
          })}
        </List>
      </Grid>
      </NormPaper>
    )
  }
}

export default withRouter(Sidebar);