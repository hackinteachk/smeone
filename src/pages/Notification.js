import React, {Component} from 'react'
import {Grid, Typography, Button} from '@material-ui/core'

class Notification extends Component {
  constructor(props) {
    super(props)
    this.state = {
      notif: [
        {
          title: 'สัมนา รู้ก่อนใคร! เทรน SME ...',
          date: '20 กรกฎาคม 2561 ',
          read: false
        },
        {
          title: "บอกเทคนิคเตรียมความพร้อมก่อนลุย...",
          date: '17 กรกฎาคม 2561',
          read: false,
        },
        {
          title: 'สัมนา รู้ก่อนใคร! เทรน SME ...',
          date: '1 กรกฎาคม 2561',
          read: true
        }
      ]
    }
  }

  render() {
    const {notif} = this.state
    return (
      <Grid container align={"center"}
            justify={"center"}
            style={{marginTop:'2em'}}>
        <Grid item xs={12}>
          <Typography variant={"display1"}>
            NOTIFICATIONS
          </Typography>
        </Grid>
        <Grid item xs={12}>
          <Grid container direction={"column"}
                spacing={24} style={{padding: '3em'}}
                align={"center"}
                justify={"center"}
          >
            <Grid item xs={2}/>
            <Grid item xs={8}>
              {Object.keys(notif).map(i => {
                const {title, date, read} = notif[i]
                return (
                  <Grid container key={i}
                        justify={"flex-start"}
                        align={"flex-start"}
                        wrap={"nowrap"}
                        style={{border: '1px solid rgba(0,0,0,0.4)', padding: '1em 2em', margin: '1em'}}>
                    <Grid item xs={8}>
                      <Typography
                        variant={"headline"}
                        color={"secondary"}
                        style={{textAlign: 'left'}}
                      >
                        {title}
                      </Typography>
                      <br/>
                      <Typography
                        variant={"caption"}
                        color={"primary"}
                        style={{textAlign: 'left'}}
                      >
                        {date}
                      </Typography>
                    </Grid>
                    <Grid
                      item xs={4}
                      container alignItems={"center"}
                      justify={"center"}
                      style={{padding: '1em'}}>
                      {!read &&
                      <div style={{
                        display: 'block',
                        backgroundColor: '#70AB95',
                        height: '10px',
                        width: '10px',
                        borderRadius: '50%'
                      }}/>}
                    </Grid>
                  </Grid>
                )
              })}
            </Grid>
            <Grid item xs={2}/>
          </Grid>
        </Grid>
        <Grid item xs={12}>
          <Button size={"large"}
                  style={{color: '#70AB95', boderRadius: '40px', borderColor: '#70AB95'}}
                  variant={"outlined"}>
            ดูเพิ่มเติม
          </Button>
        </Grid>
      </Grid>
    )
  }
}

export default Notification;