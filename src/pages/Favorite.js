import React, {Component} from 'react'
import Sidebar from './Sidebar'
import {Grid, Typography, Button, Chip, TextField} from '@material-ui/core'
import {Favorite} from '@material-ui/icons'

class FavoritePage extends Component {
  constructor(props) {
    super(props)
    this.state = {
      fav: [
        {
          text: 'โดดเด่นที่ไอเดีย 5 แฟรนไชส์สุดเจ๋ง น่าจับตามองในวงการของไทย',
          date: '20 มิ.ย.​2561'
        },
        {
          text: 'บอกเทคนิคเตรียมความพร้อม ก่อนลุยแฟรนไชส์',
          date: '20 มิ.ย.​2561'
        },
        {
          text: 'เทรนด์ธุรกิจมาแรง 2020 - อาชีพและธุรกิจที่น่าสนใจในอนาคต',
          date: '20 มิ.ย.​2561'
        },
        {
          text: `ผลสำรวจชี้ 'เอสเอมอี' จดนิติบัคคลแล้วรุ่ง ยอดขายพุ่งทะลุ 40%`,
          date: '20 มิ.ย.​2561'
        },
        {
          text: 'เวิร์คช็อป "ดีไซน์พอร์ต ออกแบบฝัน" ภายใต้กิจกรรม YDC WEEKEND (WORK)SHOP',
          date: '20 มิ.ย.​2561'
        },
      ]
    }

  }

  render() {
    const {fav} = this.state;
    return (
      <div>
        <Grid container alignItems={"center"} justify={"center"} style={{marginTop: '3em'}}>
          <Grid item xs={12}>
            <Typography variant={"display1"}>
              โครงการที่ติดตาม
            </Typography>
          </Grid>
        </Grid>
        <Grid container alignItems={"center"} style={{padding: '3em'}} spacing={24}>
          <Grid container align={"center"} wrap={"wrap"} justify={"center"}>
            <Grid item xs={4} style={{padding: '2em'}}>
              <Sidebar current={"favorite"}/>
            </Grid>
            <Grid item xs={8}>
              <Grid container direction={"row"}
                    spacing={24} style={{padding: '3em'}}>
                {Object.keys(fav).map(i=>{
                  const {text,date} = fav[i]
                  return (
                    <Grid container key={i}
                          justify={"flex-start"}
                          align={"flex-start"}
                          wrap={"nowrap"}
                          style={{border: '1px solid #70AB95', padding: '1em 2em', margin: '1em'}} >
                      <Grid item xs={8}>
                        <Typography
                          variant={"headline"}
                          color={"secondary"}
                          style={{textAlign:'left'}}
                          >
                          {text}
                        </Typography>
                        <br/>
                        <Typography
                          variant={"caption"}
                          color={"primary"}
                          style={{textAlign:'left'}}
                        >
                          {date}
                        </Typography>
                      </Grid>
                      <Grid item xs={4} style={{padding: '1em'}}>
                        <Favorite style={{color:"#FF4033"}}/>
                      </Grid>
                    </Grid>
                  )
                })}
              </Grid>
              <Grid container align={"center"} justify={"center"}>
                <Button size={"large"}
                        style={{color:'#70AB95', boderRadius: '40px', borderColor:'#70AB95'}}
                        variant={"outlined"}>
                  ดูเพิ่มเติม
                </Button>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </div>
    )
  }
}

export default FavoritePage;