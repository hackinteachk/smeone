import React, {Component} from 'react'
import Sidebar from './Sidebar'
import {Grid, Typography, Button} from '@material-ui/core'

class Profile extends Component {
  constructor(props) {
    super(props)
    this.state = {
      profile: [
        {
          title: "ชื่อ-นามสกุล",
          value: 'ภคกุล เสนีย์วงศ์ ณ​ อยุธยา',
          key: 'name'
        },
        {
          title: 'เลขประจำตัวประชาชน',
          value: '14995003486944',
          key: 'nationalId'
        },
        {
          title: 'เลขที่ผู้ประกอบการ',
          value: '12348596845944',
          key: 'smeId'
        },
        {
          title: 'ที่อยู่ผู้ประกอบการ',
          value: 'ไอเดียป็อป จำกัด 161/1 อาคารเอสจี ทาวเวอร์ ชั้น 5 ห้อง 506 ซ.มหาดเล็กหลวง 3 ถ.ราชดำริ แขวงลุมพินี เขตปทุมวัน กรุงเทพ 10330',
          key: 'address'
        },
        {
          title: 'อีเมล',
          value: 'pakakol.s@gmail.com',
          key: 'email',
        },
        {
          title: 'เบอร์โทร',
          value: '088-888-8888',
          key: 'tel'
        }
      ]
    }
  }

  render() {
    const {profile} = this.state;
    return (
      <div>
        <Grid container alignItems={"center"} justify={"center"} style={{marginTop: '3em'}}>
          <Grid item xs={12}>
            <Typography variant={"display1"}>
              ประวัติ SME ของคุณ
            </Typography>
          </Grid>
        </Grid>
        <Grid container alignItems={"center"} style={{padding: '3em'}} spacing={24}>
          <Grid container align={"center"} wrap={"wrap"} justify={"center"}>
            <Grid item xs={4} style={{padding: '2em'}}>
              <Sidebar current={"account"}/>
            </Grid>
            <Grid item xs={8}>
              <Grid container direction={"column"} wrap={"wrap"} spacing={24} style={{padding:'3em'}}>
                {Object.values(profile).map(({title, value, key}) => {
                  return (
                    <Grid container direction={"row"}
                          wrap={"wrap"} alignItems={"flex-start"}
                          style={{margin:'1em'}}
                          justify={"flex-start"} key={key}>
                      <Grid item xs={6} style={{textAlign: "left"}}>
                        <Typography gutterBottom={true} variant={"subheading"}>
                          {title}:
                        </Typography>
                      </Grid>
                      <Grid item xs={6} style={{textAlign: "left"}}>
                        <Typography gutterBottom={true} variant={"body1"}>
                          {value}
                        </Typography>
                      </Grid>
                    </Grid>
                  )
                })}
              </Grid>
              <Grid container align={"center"} justify={"center"}>
                <Button size={"large"} style={{color:'#70AB95', boderRadius: '40px', borderColor:'#70AB95'}} variant={"outlined"}>แก้ไขข้อมูลเพิ่มเติม</Button>
              </Grid>
            </Grid>
          </Grid>
        </Grid>

      </div>
    )
  }
}

export default Profile;