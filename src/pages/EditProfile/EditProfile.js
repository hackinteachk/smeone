import React, {Component} from 'react';
import {withStyles} from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Typography from "@material-ui/core/Typography";
import Section1 from './Component/Section1'
import Section2 from './Component/Section2'
import Section3 from './Component/Section3'
import Section4 from './Component/Section4'
import Section5 from './Component/Section5'
import _ from 'lodash'
import Checkbox from '@material-ui/core/Checkbox'
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Button from '@material-ui/core/Button'
import TextField from '@material-ui/core/TextField';
import moment from 'moment'
import banner from '../../static/img/banner.jpg';

const styles = theme => ({
  root: {
    'font-family': 'Kanit',
    flexGrow: 1,
    padding: theme.spacing.unit * 2,
    marginTop: '3em',
  },
  container: {
    display: 'flex',
    flexWrap: 'Wrap',
  },
  paper: {
    // padding: theme.spacing.unit * 2,
    margin: theme.spacing.unit * 2,
    textAlign: 'Center',
    color: theme.palette.text.secondary,
  },
  head: {
    'font-family': 'Kanit',
    'color': '#70AB95',
  },
  title: {
    'font-family': 'Kanit',
    color: '#494949'
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    minWidth: 200,
    maxWidth: 350,
  },
  menu: {
    width: 200,
  },
  margin: {
    margin: theme.spacing.unit
  },
  padding: {
    padding: theme.spacing.unit
  },
  formControl: {
    margin: theme.spacing.unit * 3,
  },
});


class EditProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      MEMBER_ID: '',
      MEMBER_TYPE: '',
      MEMBER_OSMEPID: '',
      MEMBER_STATUS: '',
      OWNER_CITYZEN_ID: '',
      ADDRESS_AS_CURRENT: '',
      MEMBER_TITLE: '',
      FIRSTNAME: '',
      LASTNAME: '',
      ADDRESS_NO: '',
      MOO: '',
      FLOOR: '',
      SOI: '',
      STREET: '',
      SUBDISTRICT: '',
      DISTRICT: '',
      PROVINCE: '',
      POSTCODE: '',
      TELEPHONE: '',
      MOBILE: '',
      FAX: '',
      EMAIL: '',
      FIRM_TITLE: '',
      FIRM_NAME: '',
      FIRM_ADDRESS_NO: '',
      FIRM_MOO: '',
      FIRM_SOI: '',
      FIRM_STREET: '',
      FIRM_SUBDISTRICT: '',
      FIRM_DISTRICT: '',
      FIRM_PROVINCE: '',
      FIRM_POSTCODE: '',
      FIRM_TELEPHONE: '',
      FIRM_FAX: '',
      FIRM_WEBSITE: '',
      FIRM_FACEBOOK: '',
      FIRM_IG: '',
      FIRM_EMAIL1: '',
      FIRM_EMAIL2: '',
      FIRM_TYPE: '',
      FIRM_NO: '',
      FIRM_SUB_TYPE: '',
      TSIC_Sector: '',
      TSIC_2DG: '',
      TSIC_5DG: '',
      FIRM_REGIS_YEAR: '',
      EMPLOYEE_TOTAL: '',
      FIXED_ASSET_TOTAL: '',
      OTOP_YN: null,
      PRODUCT_DESC: '',
      MATERIAL_DESC: '',
      Quest81: '',
      Quest82: '',
      Quest83: '',
      Quest300: '',
      INTEREST: {
        financial: false,
        marketing: false,
        tech: false,
        standardise: false,
        packaging: false,
        productivity: false,
        startup: false,
        advantages: false,
        newbus: false,
        knowledge: false,
        activity: false,
      },
      SUBSCRIBE: false,

      password: '',
      confirmPassword: '',
      corpAsAddress: false,
      sameAsCorpAddr: false,
    }
  }

  handleChange = name => event => {
    this.setState({
      [name]: event.target.value,
    })
  };

  getTime = () => {
    return moment().format('DD/MM/YYYY')
  }

  componentWillMount() {
    // getProvince()
    this.setState({
      time: moment().format('DD/MM/YYYY')
    })
  }

  handleSelect = name => event => {
    this.setState({
      INTEREST:
        _.assign(this.state.INTEREST, {[name]: event.target.checked})
    })
  };

  handleCheck = name => event => {
    this.setState({
      [name]: event.target.checked,
    })
  };

  render() {
    const {classes} = this.props;
    // console.log(time)
    return (
      <div>
        <img src={banner} width={"100%"}/>
        <div className={classes.root}>
          <Grid container spacing={24}>
            <Grid container direction={"row"} justify={"center"}>
              <Grid item xs={12} className={classes.paper}>
                <Typography align={"center"} variant={"title"}>
                  แก้ไขประวัติ SME ของคุณ
                </Typography>
              </Grid>
            </Grid>
            <Grid container direction={"row"} justify={"center"} style={{margin: '2em'}}>
              <Grid item xs={4}>
                <Typography align={"left"} variant={"body1"}>
                  กรณีสอบถามข้อมูลการขอรับเงิินอุดหนุน ติดต่อ สอบถามข้อมูลเพิ่มเติม ติดต่อศูนย์ OSS สสว.
                  โทร.​02-298-3000
                </Typography>
              </Grid>
              <Grid item xs={4} align={"right"}>
                <Typography align={"right"} variant={"body1"}>
                  วันที่ยื่นคำขอ<br/> วัน/เดือน/ปี
                </Typography>
                <TextField
                  value={moment().format("DD/MM/YYYY")}
                  disabled
                  type={"text"}
                />
              </Grid>
            </Grid>
            <Grid item md={2}/>
            <Grid item md={8} xs={12}>
              <Paper className={classes.paper} elevation={0}>
                <Grid container spacing={24}>
                  <Grid item xs={12} className={classes.margin}>
                    <Typography className={classes.head} variant={"title"} align={"left"}>
                      ส่วนที่ 1 : ข้อมูลพื้นฐาน
                    </Typography>
                  </Grid>
                  <Section1 classes={classes} state={this.state} hc={this.handleChange}/>
                  <Section2 classes={classes} state={this.state} hc={this.handleChange}/>
                  <Section3 classes={classes} state={this.state} hc={this.handleChange} hcheck={this.handleCheck}/>

                </Grid>
                <Grid container spacing={24}>
                  <Grid item xs={12} className={classes.margin}>
                    <Typography className={classes.head} variant={"title"} align={"left"}>
                      ส่วนที่ 2 : ข้อมูลเกี่ยวกับการดำเนินการ
                    </Typography>
                    <Typography variant={"caption"} align={"left"}>
                      (โปรดระบุให้ครบถ้วน มีผลต่อการิจารณาเข้าร่วมโครงการ)
                    </Typography>
                  </Grid>
                  <Section4 classes={classes} state={this.state} hc={this.handleChange}/>
                </Grid>
                <Grid container spacing={24}>
                  <Grid item xs={12} className={classes.margin}>
                    <Typography className={classes.head} variant={"title"} align={"left"}>
                      ส่วนที่ 3 : ข้อมูลเพิ่มเติม
                    </Typography>
                  </Grid>
                  <Section5 classes={classes} state={this.state} hc={this.handleSelect}/>
                </Grid>
              </Paper>
              <Grid container justify={"center"} alignItems={"center"}>
                <FormGroup row>
                  <FormControlLabel
                    control={
                      <Checkbox
                        checked={this.state["SUBSCRIBE"]}
                        onChange={this.handleCheck("SUBSCRIBE")}
                      />
                    }
                    label={"ท่านยินดีที่จะรับข่าวสารจาก SME ONE"}
                  />
                </FormGroup>
              </Grid>
              <Grid container justify={"center"} className={classes.margin} alignItems={"center"}>
                <Button size="large" className={classes.margin} variant="flat"
                        style={{backgroundColor: "#70AB95", color: "white"}}>บันทึก</Button>
                <Button size="large" className={classes.margin} variant="flat"
                        style={{backgroundColor: "#494949", color: "white"}}>ยกเลิก</Button>
              </Grid>
            </Grid>
            <Grid item md={2}/>
          </Grid>
        </div>
      </div>
    );
  }
}

EditProfile.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(EditProfile);