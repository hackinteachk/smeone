import React, {Component} from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from "@material-ui/core/Typography";
import Card from "@material-ui/core/Card";
import CardContent from '@material-ui/core/CardContent';
import Checkbox from '@material-ui/core/Checkbox';
import FormControl from '@material-ui/core/FormControl';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';

let options = [
  {text: 'การเงิน', value: 'financial'},
  {text: 'การตลาด', value: 'marketing'},
  {text: 'เทคโนโลยีวัฒนธรรม', value: 'tech'},
  {text: 'พัฒนามาตรฐานสินค้า', value: 'standardise'},
  {text: 'พัฒนาผลิิตภัณฑ์/บรรจุภัณฑ์', value: 'packaging'},
  {text: 'เพิ่มประสิทธิภาพธุรกิิจ', value: 'productivity'},
  {text: 'ผู้ประกอบการใหม่ / START UP', value: 'startup'},
  {text: 'สิทธิประโยชน์/กฎหมาย/ภาษี', value: 'advantages'},
  {text: 'เริ่มต้นธุรกิจใหม่ / START UP', value: 'newbus'},
  {text: 'แหล่งความรู้', value: 'knowledge'},
  {text: 'ข่าวสาร/กิจกรรม', value: 'activity'}
]


class EditProf5 extends Component {

  constructor(props) {
    super(props);
    this.state = {}
  }

  render() {
    const {classes, hc: handleChange, state} = this.props;
    return (
      <Grid item xs={12}>
        <Card>
          <CardContent>
            <Grid style={{padding: '1em'}} direction={"column"} container wrap={"wrap"} justify={"center"} align={"center"}
                  alignItems={"center"} spacing={24}>
              <Grid item xs={12} className={classes.margin}>
                <Typography className={classes.title} variant={"headline"} align={"left"}>
                  7. ผลการดำเนินธุรกิจ
                </Typography>
              </Grid>
              <Grid style={{margin: '1em'}} container wrap={"wrap"} justify={"flex-start"} align={"flex-start"} spacing={24}>
                <FormControl component="div" fullWidth className={classes.formControl}>
                  <FormGroup row>
                    {
                      Object.values(options).map(({text, value}) => {
                        return (
                          <Grid item xs={3}>
                          <FormControlLabel
                            control={
                              <Checkbox
                                key={value}
                                checked={state.INTEREST[value]}
                                onChange={handleChange(value)}
                                value={value}
                              />
                            }
                            label={text}
                          />
                          </Grid>
                        )
                      })
                    }
                  </FormGroup>
                </FormControl>
              </Grid>
            </Grid>
          </CardContent>
        </Card>
      </Grid>
    )
  }
}

export default EditProf5;