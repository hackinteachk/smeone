import React, {Component} from 'react';
import Grid from '@material-ui/core/Grid';
import classNames from 'classnames'
import Typography from "@material-ui/core/Typography";
import Card from "@material-ui/core/Card";
import CardContent from '@material-ui/core/CardContent';
import TextField from '@material-ui/core/TextField';
import MenuItem from '@material-ui/core/MenuItem';
import _ from 'lodash'
import District from '../../../static/adress/districts'
import SubDistrict from '../../../static/adress/subDistricts'
import Province from '../../../static/adress/provinces'
import Checkbox from '@material-ui/core/Checkbox';
import Radio from '@material-ui/core/Radio';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import InputAdornment from '@material-ui/core/InputAdornment';


const tempMenu = [
  {
    label: "",
    value: ""
  }
];


const MyMenu = props => {
  const {state, classes, handleChange, menus, label, value, next} = props;
  const hc = v => e => {
    handleChange(v)(e);

    next(e.target.value);
  };

  return (
    <TextField
      id={value}
      select
      label={label}
      className={classes.textField}
      value={state[value]}
      SelectProps={{
        MenuProps: {
          className: classes.menu,
        }
      }}
      style={{minWidth: "100%"}}
      margin={"normal"}
      onChange={hc(value)}
    >
      {
        menus ? menus.map(t => (
          <MenuItem key={t.value} value={t.value}>
            {t.label}
          </MenuItem>
        )) : tempMenu.map(t => (
          <MenuItem key={t.value} value={t.value}>
            {t.label}
          </MenuItem>
        ))
      }
    </TextField>
  )
};

const MyTextField = props => {
  const {state, classes, label, value, handleChange} = props
  return (
    <TextField margin={"normal"}
               className={classes.textField}
               value={state[value]}
               label={label}
               multiline={value.includes("_DESC")}
               rows={"4"}
               style={{minWidth: "100%"}}
               key={"corpForm-" + value}
               onChange={handleChange(value)}
    />
  )
}

class EditProfSec3 extends Component {

  constructor(props) {
    super(props);
    this.state = {
      corpProvinceMenu: this.getProvince(),
      corpDistrictMenu: tempMenu,
      corpSubDistrictMenu: tempMenu,
      provinceMenu: this.getProvince(),
      districtMenu: tempMenu,
      subDistrictMenu: tempMenu,
      corpAddress: {
        first: [{
          label: "ชื่อสถานประกอบการ",
          value: "FIRM_NAME",
          type: "text",
        }],
        second: [
          {
            label: "เลขที่/อาคาร",
            value: "FIRM_ADDRESS_NO",
            type: "text",
          },
          {
            label: "หมู่",
            value: "FIRM_MOO",
            type: "text",
          },
          {
            label: "ซอย",
            value: "FIRM_SOI",
            type: "text",
          }
        ],
        third: {
          road: {
            label: "ถนน",
            value: "FIRM_STREET",
            type: "text",
          },
          province: {
            label: "จังหวัด",
            value: "FIRM_PROVINCE",
            type: "menu",
            next: this.getDistrict('corpDistrictMenu'),
            menu: "corpProvinceMenu",
          },
          district: {
            label: "อำเภอ/เขต",
            value: "FIRM_DISTRICT",
            type: "menu",
            next: this.getSubDistrict('corpSubDistrictMenu'),
            menu: "corpDistrictMenu",
          }
        },
        forth: {
          sd: {
            label: "ตำบล",
            value: "FIRM_SUBDISTRICT",
            type: "menu",
            next: () => {
            },
            menu: "corpSubDistrictMenu"
          },
          post: {
            label: "รหัสไปรษณีย์",
            value: "FIRM_POSTCODE",
            type: "text"
          },
          since: {
            label: "ปีที่จัดตั้งกิจการ(พ.ศ.)",
            value: "FIRM_REGIS_YEAR",
            type: "text"
          }
        }
        ,
        fifth: [
          {
            label: "โทรศัพท์",
            value: "FIRM_TELEPHONE",
            type: "text"
          },
          {
            label: "อีเมลหลัก",
            value: "FIRM_EMAIL1",
            type: "text"
          }
        ]
      },
      address: {
        second: {
          floor: {
            label: "ชั้น",
            value: "FLOOR",
            type: "text",
          },
          num: {
            label: "เลขที่/อาคาร",
            value: "ADRESS_NO",
            type: "text",
          },
        },
        pf: {
          soi: {
            label: "ซอย",
            value: "SOI",
            type: "text",
          },
          moo: {
            label: "หมู่",
            value: "MOO",
            type: "text",
          }
        },
        third: {
          road: {
            label: "ถนน",
            value: "STREET",
            type: "text",
          },
          province: {
            label: "จังหวัด",
            value: "PROVINCE",
            type: "menu",
            next: this.getDistrict('districtMenu'),
            menu: "provinceMenu",
          },
          district: {
            label: "อำเภอ/เขต",
            value: "DISTRICT",
            type: "menu",
            next:this.getSubDistrict('subDistrictMenu'),
            menu: "districtMenu",
          }
        },
        forth: {
          sd: {
            label: "ตำบล/แขวง",
            value: "SUBDISTRICT",
            type: "menu",
            next: () => {
            },
            menu: "subDistrictMenu"
          },
          post: {
            label: "รหัสไปรษณีย์",
            value: "POSTCODE",
            type: "text"
          },
          tel: {
            label: "โทรศัพท์",
            value: "TELEPHONE",
            type: "text"
          }
        }
      },
      companyRegisterType: [
        [
          {
            label: "ภาคธุรกิจ",
            value: "TSIC_Sector",
            type: "menu"
          },
          {
            label: "ธุรกิจ",
            value: "TSIC_2DG",
            type: "menu"
          },
          {
            label: "รูปแบบธุรกิจ",
            value: "TSIC_5DG",
            type: "menu"
          }
        ],
        [
          {
            label: "ระบุรายละเอียดสินค้า หรือบริการโดยละเอียด (ยี่ห้อ ผลิตภัณฑ์​รูปทรง​ฯลฯ)",
            value: "PRODUCT_DESC",
            type: "text"
          }
        ],
        [
          {
            label: "วัตถุดิบหลักที่ใช้ผลิตสินค้า/บริการ",
            value: "MATERIAL_DESC",
            type: "text"
          }
        ]
      ],
      companyType: [
        [
          {
            label: "กรณีเป็นนิติบุคคล",
            value: "CORP_YN",
            type: "menu",
          },
          {
            label: "เลขทะเบียนนิติบุคคล",
            value: "CORP_ID",
            type: "menu",
          }
        ]
      ]
    }
  }

  getProvince = () => {
    let arr = _.map(Province, (p) => {
      let red = _.reduce(p, (res, value, key) => {
        if (key === "PROVINCE_ID") {
          res["value"] = value
        }
        else if (key === "PROVINCE_NAME") {
          res["label"] = value
        }
        else if (key === "PROVINCE_ID") {
          res["id"] = value
        }
        return res
      }, {});
      return red;
    });
    return arr;
  };

  getDistrict = target => province_id => {
    let a = this.getAddress({
      json: District,
      parent: "PROVINCE_ID",
      label: "DISTRICT_NAME",
      value: "DISTRICT_ID"
    })(province_id);

    this.setState({
      [target]: a
    });
  };

  getSubDistrict = target => district_id => {
    let a = this.getAddress({
      json: SubDistrict,
      parent: "DISTRICT_ID",
      label: "SUB_DISTRICT_NAME",
      value: "SUB_DISTRICT_ID",
    })(district_id);

    this.setState({
      [target]: a
    })
  };

  getAddress = ({json, parent, label, value}) => parent_id => {
    let tmp = _.filter(json, item => {
      return item[parent].toString() === parent_id.toString();
    });

    let lll = _.reduce(tmp, (res, val) => {
      res.push(_.reduce(val, (r, v, k) => {
        if (k === label) {
          r["label"] = v
        }

        if (k === value) {
          r["value"] = v
        }
        return r
      }, {}))
      return res
    }, []);
    // console.log(lll)
    return lll;
  };

  render() {
    const {classes, hcheck: handleCheck, hc: handleChange, state,} = this.props;
    const {address, companyRegisterType: crt, companyType} = this.state;
    return (
      <Grid item xs={12}>
        <Card>
          <CardContent>
            {/* 2 */}
            <Grid style={{padding: '1em'}} container wrap={"wrap"} justify={"center"} align={"center"}
                  alignItems={"center"} spacing={24}>
              <Grid item xs={12} className={classes.margin}>
                <Typography className={classes.title} variant={"headline"} align={"left"}>
                  2. ที่อยู่สถานประกอบการ​(เฉพาะสำนักงานใหญ่หรือกิจการอย่างเดียว)
                </Typography>
              </Grid>
              <Grid item xs={12}>
                <Grid container>
                  <Grid item xs={12}>
                    <form autoComplete={"off"} className={classes.container}>
                      <Grid container>
                        {Object.values(this.state["corpAddress"]).map(v => {
                          const len = Object.values(v).length;
                          return (
                            Object.values(v).map(({label, value, type, ...rest}) => (
                              type === "text" ?
                                <Grid item xs={12} md={12 / len}
                                      key={"inputCorpGrid-" + value} className={classes.padding}>
                                  <MyTextField state={state} classes={classes} label={label} value={value}
                                               handleChange={handleChange}/>
                                </Grid>
                                :
                                <Grid item xs={12} md={12 / len} key={"inputCorpGrid-" + value}
                                      className={classes.padding}
                                >
                                  <MyMenu state={state} classes={classes}
                                          handleChange={handleChange}
                                          label={label} value={value} menus={this.state[rest.menu]}
                                          next={rest.next}
                                  />
                                </Grid>
                            ))
                          )
                        })}
                        <Grid item xs={12} key={"inputCorpGrid-corpAsAddress"} align={"left"}>
                          <FormControlLabel
                            control={
                              <Checkbox
                                value={"corpAsAddress"}
                                checked={state["corpAsAddress"]}
                                onChange={handleCheck("corpAsAddress")}/>
                            }
                            label="ใช้เป็นที่อยู่ติดต่อกลับ"
                            style={{fontFamily: "kanit"}}
                            className={classes.margin}
                          />
                        </Grid>
                      </Grid>
                    </form>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>


            {/* 3 */}
            <Grid style={{padding: '1em'}} container wrap={"wrap"} justify={"center"} align={"center"}
                  alignItems={"center"} spacing={24}>
              <Grid item xs={12} className={classes.margin}>
                <Typography className={classes.title} variant={"headline"} align={"left"}>
                  3. ที่อยู่ปัจจุบัน
                </Typography>
              </Grid>
              <Grid item xs={12}>
                <Grid container>
                  <Grid item xs={12}>
                    <form autoComplete={"off"} className={classes.container}>
                      <Grid item xs={12} key={"inputCorpGrid-corpAsAddress"} align={"left"}>
                        <FormControlLabel
                          control={
                            <Checkbox
                              value={"sameAsCorpAddr"}
                              checked={state["sameAsCorpAddr"]}
                              onChange={handleCheck("sameAsCorpAddr")}/>
                          }
                          label="ที่อยู่เดียวกับสถานประกอบการ"
                          style={{fontFamily: "kanit"}}
                          className={classes.margin}
                        />
                      </Grid>
                      <Grid container>
                        {Object.values(address).map(v => {
                          const len = Object.values(v).length;
                          return (
                            Object.values(v).map(({label, value, type, ...rest}) => (
                              type === "text" ?
                                <Grid item xs={12} md={12 / len}
                                      key={"inputCorpGrid-" + value} className={classes.padding}>
                                  <MyTextField state={state} classes={classes} label={label} value={value}
                                               handleChange={handleChange}/>
                                </Grid>
                                :
                                <Grid item xs={12} md={12 / len} key={"inputCorpGrid-" + value}
                                      className={classes.padding}
                                >
                                  <MyMenu state={state} classes={classes}
                                          handleChange={handleChange}
                                          label={label} value={value} menus={this.state[rest.menu]}
                                          next={rest.next}
                                  />
                                </Grid>
                            ))
                          )
                        })}
                      </Grid>
                    </form>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
            {/* 4 */}
            <Grid style={{padding: '1em'}} container wrap={"wrap"} justify={"center"} align={"center"}
                  alignItems={"center"} spacing={24}>
              <Grid item xs={12} className={classes.margin}>
                <Typography className={classes.title} variant={"headline"} align={"left"}>
                  4. รูปแบบการจัดตั้งกิจการ
                </Typography>
              </Grid>
              <Grid item xs={12}>
                <Grid container align={"center"}>
                  {
                    Object.values(companyType).map(arr => {
                      let len = 12 / Object.values(arr).length;
                      return (
                        Object.values(arr).map(({type, label, value, ...rest}) => (
                            type === "text" ?
                              <Grid item xs={12} md={len} className={classes.padding} key={"CorpTypeT" + value}>
                                <MyTextField state={state} classes={classes} label={label} value={value}
                                             handleChange={handleChange}/>
                              </Grid>
                              :
                              <Grid item xs={12} md={len} className={classes.padding} key={"CorpTypeB" + value}>
                                <MyMenu state={state} classes={classes}
                                        handleChange={handleChange}
                                        label={label} value={value} menus={this.state[rest.menu]}
                                        next={() => {
                                        }}
                                />
                              </Grid>
                          )
                        ))
                    })
                  }
                  <Grid container align={"center"}>

                    <Grid item xs={12} md={4} className={classes.padding} key={"regCorpSelect"}>
                      <MyMenu state={state} classes={classes}
                              handleChange={handleChange}
                              label={"กรุณาเลือก"} value={""} menus={[]}
                              next={() => {
                              }}
                      />
                    </Grid>

                    <Grid item xs={12} md={8} className={classes.padding} key={"isOtopRad"} style={{marginTop: '2em'}}>
                      <a>ท่านผลิตสินค้า OTOP หรือไม่:</a>
                      <Radio
                        checked={state.OTOP_YN === '1'}
                        value={'1'}
                        key={"otop-y"}
                        onChange={handleChange("OTOP_YN")}
                        label={"มี"}
                      />
                      <a>มี</a>
                      <Radio
                        checked={state.OTOP_YN === '0'}
                        value={'0'}
                        key={"otop-n"}
                        onChange={handleChange("OTOP_YN")}
                        aria-label={"ไม่มี"}
                      />
                      <a>ไม่มี</a>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
            {/* 5 */}
            <Grid style={{padding: '1em'}} container wrap={"wrap"} justify={"center"} align={"center"}
                  alignItems={"center"} spacing={24}>
              <Grid item xs={12} className={classes.margin}>
                <Typography className={classes.title} variant={"headline"} align={"left"}>
                  5. ประเภทธุรกิจ
                </Typography>
              </Grid>
              <Grid item xs={12}>
                <Grid container>
                  {
                    Object.values(crt).map(arr => {
                      let len = 12 / Object.values(arr).length;
                      return (
                        Object.values(arr).map(({type, label, value, ...rest}) => (
                            type === "text" ?
                              <Grid item xs={12} md={len} className={classes.padding} key={"regCorp" + value}>
                                <MyTextField state={state} classes={classes} label={label} value={value}
                                             handleChange={handleChange}/>
                              </Grid>
                              :
                              <Grid item xs={12} md={len} className={classes.padding} key={"regCorp" + value}>
                                <MyMenu state={state} classes={classes}
                                        handleChange={handleChange}
                                        label={label} value={value} menus={[]}
                                        next={() => {
                                        }}
                                />
                              </Grid>
                          )
                        ))
                    })
                  }
                </Grid>
              </Grid>
            </Grid>

            {/* 6 */}
            <Grid style={{padding: '1em'}} container wrap={"wrap"} justify={"center"} align={"center"}
                  alignItems={"center"} spacing={24}>
              <Grid item xs={12} className={classes.margin}>
                <Typography className={classes.title} variant={"headline"} align={"left"}>
                  6. จำนวนคนทำงาน ปีที่ผ่านมาล่าสุด (พนักงานประจำและรวมเจ้าของกิจการ)
                </Typography>
              </Grid>
              <Grid item xs={12}>
                <Grid container>
                  <Grid item xs={12} className={classes.padding} key={"numEmployeeGrid"}>
                    <TextField
                      id="filled-adornment-weight"
                      className={classNames(classes.margin, classes.textField)}
                      label="(พนักงานประจำและรวมเจ้าของกิจการ)"
                      value={state['EMPLOYEE_TOTAL']}
                      onChange={handleChange('EMPLOYEE_TOTAL')}
                      InputProps={{
                        endAdornment: (
                          <InputAdornment position="end">
                            คน
                          </InputAdornment>
                        ),
                      }}
                      style={{minWidth: "100%"}}
                    />
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </CardContent>
        </Card>
      </Grid>
    )
  }
}

export default EditProfSec3;