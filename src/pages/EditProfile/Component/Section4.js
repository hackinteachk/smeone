import React, {Component} from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from "@material-ui/core/Typography";
import Card from "@material-ui/core/Card";
import CardContent from '@material-ui/core/CardContent';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';

const tempMenu = [
  {
    label: "",
    value: ""
  }
];

const MyMenu = props => {
  const {state, classes, handleChange, menus, label, value} = props;

  return (
    <TextField
      id={value}
      select
      label={label}
      className={classes.textField}
      value={state[value]}
      SelectProps={{
        MenuProps: {
          className: classes.menu,
        }
      }}
      style={{minWidth: "100%"}}
      margin={"normal"}
      onChange={handleChange(value)}
    >
      {
        menus ? menus.map(t => (
          <MenuItem key={t.value} value={t.value}>
            {t.label}
          </MenuItem>
        )) : tempMenu.map(t => (
          <MenuItem key={t.value} value={t.value}>
            {t.label}
          </MenuItem>
        ))
      }
    </TextField>
  )
};

let menus = [
  {
    label: "ทุนจดทะเบียน/เงินทุนเริ่มกิจการ​(ปีที่เริ่มต้นกิจการ) หน่วยบาท",
    value: "Quest81"
  },
  {
    label: "รายได้รวมของปีที่ผ่านมาล่าสุด (หน่วยบาท)",
    value: "Quest82",
  },
  {
    label: "กำไรผลประกอบการของปีที่ผ่านมาล่าสุด (หน่วยบาท)"
  }
];

class EditProf4 extends Component {

  constructor(props) {
    super(props);
    this.state = {}
  }

  render() {
    const {classes, hc: handleChange, state} = this.props;
    return (
      <Grid item xs={12}>
        <Card>
          <CardContent>
            <Grid style={{padding: '1em'}} container wrap={"wrap"} justify={"center"} align={"center"}
                  alignItems={"center"} spacing={24}>
              <Grid item xs={12} className={classes.margin}>
                <Typography className={classes.title} variant={"headline"} align={"left"}>
                  7. ผลการดำเนินธุรกิจ
                </Typography>
              </Grid>
              <Grid style={{padding: '1em'}} container wrap={"wrap"} justify={"center"} align={"center"}
                    alignItems={"center"} spacing={24}>
                <Grid item xs={12}>
                  {
                    Object.values(menus).map(({label, value}) => {
                      return (
                        <MyMenu state={state} classes={classes}
                                handleChange={handleChange}
                                label={label} value={value}
                                menus={[]}
                        />
                      )
                    })
                  }
                </Grid>
              </Grid>
            </Grid>
          </CardContent>
        </Card>
      </Grid>
    )
  }
}

export default EditProf4;