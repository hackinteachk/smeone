import React, {Component} from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from "@material-ui/core/Typography";
import Card from "@material-ui/core/Card";
import CardContent from '@material-ui/core/CardContent';
import TextField from '@material-ui/core/TextField';
import Select from '@material-ui/core/Select';
import {FormControl, Input} from '@material-ui/core'
import NativeSelect from '@material-ui/core/NativeSelect';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';

const title = [
  {
    value: "นาย",
    label: "นาย"
  },
  {
    value: "นางสาว",
    label: "นางสาว"
  }
];

const textfield1 = [
  {
    label: "ชื่อ",
    value: "FIRSTNAME"
  },
  {
    label: "นามสกุล",
    value: "LASTNAME"
  },
  {
    label: "เลขที่บัตรประจำตัวประชาชน",
    value: "OWNER_CITIZEN_ID"
  },
  {
    label: "เลขที่ผู้ประกอบการ",
    value: "FIRM_NO"
  }
];

class RegisterSect1 extends Component {
  constructor(props) {
    super(props);
    this.state = {}
  }

  render() {
    const {classes, hc: handleChange, state} = this.props;
    return (
      <Grid item xs={12}>
        <Card>
          <CardContent>
            <Grid style={{padding: '1em'}} container wrap={"wrap"} justify={"center"} align={"center"}
                  alignItems={"center"} spacing={24}>
              <Grid item xs={12} className={classes.margin}>
                <Typography className={classes.title} variant={"headline"} align={"left"}>
                  1. ชื่อ-สกุล
                </Typography>
              </Grid>
              <Grid item xs={12}>
                <form autoComplete={"off"}>
                  <Grid container align={"center"} justify={"flex-start"}>
                    <Grid item xs={6}>
                      <TextField
                        id={"title"}
                        select
                        label={"คำนำหน้าชื่อ"}
                        className={classes.textField}
                        value={state.MEMBER_TITLE}
                        SelectProps={{
                          MenuProps: {
                            className: classes.menu,
                          }
                        }}
                        margin={"normal"}
                        onChange={handleChange("MEMBER_TITLE")}
                      >
                        {title.map(t => (
                          <MenuItem key={t.value} value={t.value}>
                            {t.label}
                          </MenuItem>
                        ))}
                      </TextField>
                    </Grid>
                    <Grid item xs={6}/>
                    {textfield1.map(({value, label}) => (
                      <Grid item xs={12} sm={6} key={"container-" + value}>
                        <TextField margin={"normal"}
                                   className={classes.textField}
                                   value={state[value]}
                                   label={label}
                                   key={"form-" + value}
                                   onChange={handleChange(value)}
                        />
                      </Grid>
                    ))}
                  </Grid>
                </form>
              </Grid>
            </Grid>
          </CardContent>
        </Card>
      </Grid>

    )
  }
}

export default RegisterSect1;