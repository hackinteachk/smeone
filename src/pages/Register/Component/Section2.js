import React, {Component} from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from "@material-ui/core/Typography";
import Card from "@material-ui/core/Card";
import {TextField} from '@material-ui/core'
import CardContent from '@material-ui/core/CardContent';
import Input from '@material-ui/core/Input';
import InputAdornment from '@material-ui/core/InputAdornment';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import IconButton from '@material-ui/core/IconButton';

let textfield1 = [
  {
    label: "อีเมล",
    value: "EMAIL",
    type: "",
    show: false,
  },
  {
    label: "รหัสผ่าน",
    value: "password",
    type: "password",
    show: "showPassword"
  },
  {
    label: "ยืนยันรหัสผ่าน",
    value: "confirmPassword",
    type: "password",
    show: "showCPassword"
  }
];

class RegisterSect2 extends Component {

  constructor(props) {
    super(props);
    this.state = {
      showPassword: false,
      showCPassword: false,
    }
  }

  togglePassword = () => {
    this.setState({
      showPassword: !this.state.showPassword
    })
  }

  toggleCPassword = () => {
    this.setState({
      showCPassword: !this.state.showCPassword
    })
  }

  handleMouseDownPassword = e => {
    e.preventDefault();
  };

  render() {
    const {classes, hc: handleChange, state} = this.props;
    return (
      <Grid item xs={12} className={classes.container}>
        <Card>
          <CardContent>
            <Grid container direction={"row"}
                  spacing={24} style={{padding: '3em'}}>
              <Grid item xs={12}>
                <Typography variant={"headline"} style={{textAlign: 'left'}}>
                  ข้อมูลการเข้าใช้ระบบ
                </Typography>
              </Grid>
              <Grid item xs={12} container>

              </Grid>
              <Grid item container xs={12}>
                {/*LEFT*/}
                <Grid item xs={6}>
                  <form>
                    <Input
                      style={{mindWidth: '100%', margin: '1em'}}
                      placeholder={"อีเมล"}
                      type={"text"}
                      onChange={handleChange('EMAIL')}
                      endAdornment={
                        <InputAdornment position="end">
                          <IconButton
                          >
                          </IconButton>
                        </InputAdornment>
                      }
                    />
                    <Input
                      style={{minWidth: "50%", margin: '1em'}}
                      placeholder={"รหัสผ่าน"}
                      type={'password'}
                      value={state['password']}
                      onChange={handleChange('password')}
                      endAdornment={
                        <InputAdornment position="end">
                          <IconButton
                            aria-label="Toggle password visibility"
                            onClick={this.togglePassword}
                            onMouseDown={this.handleMouseDownPassword}
                          >
                            {this.state['showPassword'] ? <VisibilityOff/> : <Visibility/>}
                          </IconButton>
                        </InputAdornment>
                      }
                    />
                    <Input
                      style={{minWidth: "50%", margin: '1em'}}
                      placeholder={"ยืนยันรหัสผ่าน"}
                      type={'password'}
                      value={state['confirmPassword']}
                      onChange={handleChange('confirmPassword')}
                      endAdornment={
                        <InputAdornment position="end">
                          <IconButton
                            aria-label="Toggle password visibility"
                            onClick={this.toggleCPassword}
                            onMouseDown={this.handleMouseDownPassword}
                          >
                            {this.state['showCPassword'] ? <VisibilityOff/> : <Visibility/>}
                          </IconButton>
                        </InputAdornment>
                      }
                    />
                  </form>
                </Grid>
                <Grid xs={6} item container direction={"column"} justify={"flex-end"} alignItems={"baseline"}>
                  <Grid item>
                    <Typography
                      variant={"subheading"}
                      style={{textAlign: 'left', textDecoration: 'underline'}}>
                      คำแนะนำในการตั้งรหัสผ่าน
                    </Typography>
                    <br/>
                    <Typography
                      variant={"caption"}
                      style={{textAlign: 'left'}}
                    >
                      ควรมีความยาวไม่น้อยกว่า 8 ตัวอักษร หรือมากกว่านั้น
                    </Typography>
                    <Typography
                      variant={"caption"}
                      style={{textAlign: 'left'}}
                    >
                      ประกอบด้วยตัวอักษร(a-z,A-Z) / ตัวเลข (0-9)
                    </Typography>
                    <Typography
                      variant={"caption"}
                      style={{textAlign: 'left'}}
                    >
                      เครื่องหมายหรืออักขระพิเศษ (!@#$%^&*()_-=+[]?/.,)
                    </Typography>
                    <Typography
                      variant={"caption"}
                      style={{textAlign: 'left'}}
                    >
                      ตัวอย่างเช่น <a style={{color: '#70AB95'}}>#Admin1234</a>
                    </Typography>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </CardContent>
        </Card>
      </Grid>
    )
  }
}

export default RegisterSect2;