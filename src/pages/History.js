import React, {Component} from 'react'
import Sidebar from './Sidebar'
import {Grid, Typography, Button,Chip} from '@material-ui/core'

class History extends Component {
  constructor(props) {
    super(props)
    this.state = {
      history : [
        {
          title: 'เวิร์คช็อป "ดีไซน์พอร์ต ออกแบบฝัน" ภายใต้กิจกรรม YDC WEEKEND (WORK)SHOP',
          date: '20 มิ.ย. 2561',
          tag: []
        },
        {
          title: 'สัมนาฟรี! ด้านการเกษตรยุคใหม่ ไทยแลนด์ 4.0',
          date: '20 มิ.ย. 2561',
          tag: ["แบบสำรวจ"]
        },
        {
          title: 'ฟรี อบรมหลักสูตร Advance Info graphics Motion',
          date: '20 มิ.ย. 2561',
          tag: []
        },
        {
          title: 'ฟรี! สัมนา "SME เพิิ่มโกาสทางธุรกิจ พิชิตยอดขาย ด้วย Online Marketing" ',
          date: '19 มิ.ย. 2561',
          tag: []
        }
      ]
    }
  }

  render() {
    const {history} = this.state;
    return (
      <div>
        <Grid container alignItems={"center"} justify={"center"} style={{marginTop: '3em'}}>
          <Grid item xs={12}>
            <Typography variant={"display1"}>
              ประวัติเข้าร่วมโครงการ
            </Typography>
          </Grid>
        </Grid>
        <Grid container alignItems={"center"} style={{padding: '3em'}} spacing={24}>
          <Grid container align={"center"} wrap={"wrap"} justify={"center"}>
            <Grid item xs={4} style={{padding: '2em'}}>
              <Sidebar current={"history"}/>
            </Grid>
            <Grid item xs={8}>
              <Grid container direction={"row"} wrap={"wrap"} spacing={24} style={{padding:'3em'}}>
                {Object.keys(history).map(i => {
                  const {title, date, tag} = history[i]
                  return (
                    <Grid container key={i}
                          justify={"flex-start"}
                          align={"flex-start"}
                          style={{border: '1px solid #70AB95', padding: '1em 2em', margin: '1em'}} >
                      <Grid item xs={12}>
                        <Typography variant={"subheading"} style={{textAlign:'left',color:'#70AB95'}}>
                          {title}
                        </Typography>
                      </Grid>
                      <Grid item xs={12} style={{marginTop:'0.5em'}}>
                        <Grid container
                              align={"center"}
                              direction={"row"}
                              justify={"flex-start"}
                              wrap={'wrap'}>
                          <Typography variant={"body1"} style={{textAlign:'left',color:'#70AB95'}}>
                            {date}
                          </Typography>
                          {
                            Object.values(tag).map(t => {
                              return (
                                <Chip
                                  color={"secondary"}
                                  style={{color: 'white', marginLeft:'1em'}}
                                  component="div"
                                  key={`${i}+${t}`}
                                  label={t}/>
                              )
                            })
                          }
                        </Grid>

                      </Grid>
                    </Grid>
                  )
                })}
              </Grid>
              <Grid container align={"center"} justify={"center"}>
                <Button size={"large"}
                        style={{color:'#70AB95', boderRadius: '40px', borderColor:'#70AB95'}}
                        variant={"outlined"}>
                  ดูเพิ่มเติม
                </Button>
              </Grid>
            </Grid>
          </Grid>
        </Grid>

      </div>
    )
  }
}

export default History;